# 🐷📊 BACON Window stats

[BAckport and CONfig](https://wikitech.wikimedia.org/wiki/Backport_windows) (BACON) windows have happened for a long time.

This repo gathers data about the windows going back for the past 5 years.


```python
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

from sqlalchemy import create_engine
    
engine = create_engine('sqlite:///data/bacon.db')
df = pd.read_sql('''
SELECT
    strftime('%H', datetime, 'unixepoch') as hour,
    count(url) as patches
FROM window w
join patch_window pw on w.id = pw.window_id
join patch p on p.id = pw.window_id
group by hour
''', engine)

# Makes your data 538% better...I think
plt.style.use('fivethirtyeight')
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>hour</th>
      <th>patches</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>00</td>
      <td>548</td>
    </tr>
    <tr>
      <th>1</th>
      <td>11</td>
      <td>738</td>
    </tr>
    <tr>
      <th>2</th>
      <td>12</td>
      <td>379</td>
    </tr>
    <tr>
      <th>3</th>
      <td>13</td>
      <td>551</td>
    </tr>
    <tr>
      <th>4</th>
      <td>14</td>
      <td>321</td>
    </tr>
    <tr>
      <th>5</th>
      <td>15</td>
      <td>284</td>
    </tr>
    <tr>
      <th>6</th>
      <td>16</td>
      <td>165</td>
    </tr>
    <tr>
      <th>7</th>
      <td>17</td>
      <td>79</td>
    </tr>
    <tr>
      <th>8</th>
      <td>18</td>
      <td>766</td>
    </tr>
    <tr>
      <th>9</th>
      <td>19</td>
      <td>464</td>
    </tr>
    <tr>
      <th>10</th>
      <td>23</td>
      <td>1271</td>
    </tr>
  </tbody>
</table>
</div>




```python
df.set_index('hour').plot(kind='bar')
plt.title('Patches deploy in backports (2016–2021)')
```




    Text(0.5, 1.0, 'Patches deploy in backports (2016–2021)')




![png](README_files/README_2_1.png)


# What are these outliers?


```python
df = pd.read_sql('''
SELECT
    distinct strftime('https://wikitech.wikimedia.org/wiki/Deployments/Archive/%Y/%m', datetime, 'unixepoch') as link,
    strftime('%H', datetime, 'unixepoch') as hour
FROM window w
join patch_window pw on w.id = pw.window_id
join patch p on p.id = pw.patch_id
WHERE hour == '17'
''', engine)

from IPython.display import display, Markdown
for link in df['link']:
    print(link)
```

    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2018/03
    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2018/04
    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2018/05
    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2018/06
    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2018/11
    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2018/12
    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2019/01
    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2019/02
    https://wikitech.wikimedia.org/wiki/Deployments/Archive/2019/03


I guess ^ all look legit...

## What about names...


```python
def remap_name(name):
    name = name.lower()
    if 'mid-day' in name:
        return 'utc-morning (11:00)'
    if 'morning' in name:
        return 'utc-evening (16:00)'
    if 'evening' in name:
        return 'utc-late (23:00)'

df = pd.read_sql('''
SELECT
    name,
    count(url) as patches
FROM window w
join patch_window pw on w.id = pw.window_id
join patch p on p.id = pw.patch_id
group by name
''', engine)

df['name']=df['name'].map(remap_name)
df.groupby('name').sum()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>patches</th>
    </tr>
    <tr>
      <th>name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>utc-evening (16:00)</th>
      <td>1758</td>
    </tr>
    <tr>
      <th>utc-late (23:00)</th>
      <td>1819</td>
    </tr>
    <tr>
      <th>utc-morning (11:00)</th>
      <td>1989</td>
    </tr>
  </tbody>
</table>
</div>




```python
df = df.groupby('name').sum().plot(kind='bar')
plt.title('Patches per BACON Window')
```




    Text(0.5, 1.0, 'Patches per BACON Window')




![png](README_files/README_7_1.png)


How is it possible that we deploy the most patches at 23:00 hours, but utc-late is the least popular...?


```python
df = pd.read_sql('''
SELECT
    name,
    strftime('%H', datetime, 'unixepoch') as hour,
    count(url) as patches
FROM window w
JOIN patch_window pw on w.id = pw.window_id
JOIN patch p on p.id = pw.patch_id
GROUP BY hour
ORDER BY name, patches
''', engine)
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>name</th>
      <th>hour</th>
      <th>patches</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>European Mid-day SWAT(Max 6 patches)</td>
      <td>12</td>
      <td>379</td>
    </tr>
    <tr>
      <th>1</th>
      <td>European Mid-day SWAT(Max 6 patches)</td>
      <td>11</td>
      <td>738</td>
    </tr>
    <tr>
      <th>2</th>
      <td>European Mid-day SWAT(Max 8 patches)</td>
      <td>14</td>
      <td>321</td>
    </tr>
    <tr>
      <th>3</th>
      <td>European Mid-day SWAT(Max 8 patches)</td>
      <td>13</td>
      <td>551</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Evening SWAT (Max 8 patches)</td>
      <td>00</td>
      <td>548</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Evening SWAT (Max 8 patches)</td>
      <td>23</td>
      <td>1271</td>
    </tr>
    <tr>
      <th>6</th>
      <td>Morning SWAT (Max 8 patches)</td>
      <td>17</td>
      <td>79</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Morning SWAT (Max 8 patches)</td>
      <td>16</td>
      <td>165</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Morning SWAT (Max 8 patches)</td>
      <td>19</td>
      <td>464</td>
    </tr>
    <tr>
      <th>9</th>
      <td>Morning SWAT(Max 8 patches)</td>
      <td>15</td>
      <td>284</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Morning SWAT(Max 8 patches)</td>
      <td>18</td>
      <td>766</td>
    </tr>
  </tbody>
</table>
</div>



I guess ^ is why: we rarely moved `Evening SWAT` but everything else kinda moves around


```python

```
