#!/usr/bin/env python3

from datetime import datetime, timedelta
import requests
from bs4 import BeautifulSoup, NavigableString
import sqlite3
import dateparser
import time
import pytz

BASE_URL = 'https://wikitech.wikimedia.org/wiki/Deployments/Archive'
LAST_MONTH = datetime.utcnow() - timedelta(days=30)
FIVE_YEARS_AGO = LAST_MONTH - timedelta(days=365*5)
DB_PATH = './data/bacon.db'


def parse_all():
    windows = {}
    for year in range(FIVE_YEARS_AGO.year, LAST_MONTH.year + 1):
        for month in range(1, 13):
            if year == 2021 and month > 3:
                continue  # we'll figure out the new format later
            print(f'Processing {year}-{month:02d}')
            r = requests.get(f'{BASE_URL}/{year}/{month:02d}')
            soup = BeautifulSoup(r.text, 'html.parser')
            for window in soup.find_all('td'):
                if 'Requesting Developer' not in window.text:
                    continue
                window_text = window.findParent('tr').text.lower()

                if not ('swat' in window_text or 'backport window' in window_text):
                    continue
                if 'puppet' in window_text:
                    continue

                developers = window.find_all(class_='ircnick-container')
                for developer in developers:
                    paren = developer.findParent('tr')
                    time = paren.find(class_='deploycal-time-utc')\
                        .find(class_='deploycal-starttime')['datetime']
                    name = paren.find(class_='deploycal-window').text

                    # Sometimes they're wrapped in a "<p>" sometimes not
                    if developer.parent.name == 'p':
                        developer = developer.parent

                    developer_name = developer.text.strip()

                    if developer_name == 'Requesting Developer (irc-nickname)':
                        continue

                    for sib in developer.next_siblings:
                        patches = windows.setdefault(f'{name}⚡{time}', {})\
                            .setdefault(developer_name, [])

                        # Skip newlines
                        if isinstance(sib, NavigableString):
                            continue

                        # This is the next dev in the list
                        irc = sib.find(class_='ircnick-container')
                        if irc is not None and not isinstance(irc, int):
                            # print(f'ircnick-container: "{sib.text.strip()}"')
                            break

                        patches += [
                            patch['href'] for patch in sib.find_all('a')
                            if patch['href'].startswith('https://gerrit.wikimedia')
                        ]
    return windows


def setup_db():
    """
    setup the database file if it doesn't exist
    """
    conn = sqlite3.connect(DB_PATH)
    # If the db didn't exist at the start of the function, do the setup
    crs = conn.cursor()

    crs.execute('''
        CREATE TABLE IF NOT EXISTS window (
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            datetime INTEGER UNIQUE NOT NULL
        );
    ''')
    crs.execute('''
        CREATE TABLE IF NOT EXISTS developer (
            id INTEGER PRIMARY KEY,
            name TEXT UNIQUE NOT NULL
        );
    ''')
    crs.execute('''
        CREATE TABLE IF NOT EXISTS patch (
            id INTEGER PRIMARY KEY,
            url TEXT NOT NULL
        );
    ''')

    # Windows can have many developers
    # Developers can be in many windows
    crs.execute('''
        CREATE TABLE IF NOT EXISTS developer_window (
            id INTEGER PRIMARY KEY,
            developer_id INTEGER NOT NULL,
            window_id INTEGER NOT NULL,
            UNIQUE(developer_id, window_id),
            FOREIGN KEY(developer_id) REFERENCES developer(id),
            FOREIGN KEY(window_id) REFERENCES window(id)
        )
    ''')
    # Developers can have many patches
    # Patches may be deployed by multiple developers (if a developer can't get it deployed, for example)
    crs.execute('''
        CREATE TABLE IF NOT EXISTS developer_patch (
            id INTEGER PRIMARY KEY,
            developer_id INTEGER NOT NULL,
            patch_id INTEGER NOT NULL,
            UNIQUE(developer_id, patch_id),
            FOREIGN KEY(developer_id) REFERENCES developer(id),
            FOREIGN KEY(patch_id) REFERENCES patch(id)
        )
    ''')
    # Windows can have many patches
    # Patches may be deployed in multiple windows (if a developer can't get it deployed, for example)
    crs.execute('''
        CREATE TABLE IF NOT EXISTS patch_window (
            id INTEGER PRIMARY KEY,
            patch_id INTEGER NOT NULL,
            window_id INTEGER NOT NULL,
            UNIQUE(patch_id, window_id),
            FOREIGN KEY(patch_id) REFERENCES patch(id),
            FOREIGN KEY(window_id) REFERENCES window(id)
        )
    ''')

    conn.commit()
    return conn


if __name__ == '__main__':
    conn = setup_db()
    crs = conn.cursor()
    seen_developers = set()
    seen_patches = set()
    for window_name, developer_patches in parse_all().items():
        wn_ts = [x.strip() for x in window_name.split('⚡')]
        ts = int(dateparser.parse(wn_ts[-1]).timestamp())

        crs.execute('''
            INSERT INTO window(
                datetime,
                name
            )VALUES(?,?)''', (ts,wn_ts[0])
        )

        window_id = crs.lastrowid

        for developer, patches in developer_patches.items():
            old_dev_len = len(seen_developers)
            seen_developers.add(developer)

            if len(seen_developers) != old_dev_len:
                crs.execute('''
                    INSERT INTO developer(
                        name
                    )VALUES(?)''', (developer,)
                )
                developer_id = crs.lastrowid
            else:
                developer_id = crs.execute(
                    'SELECT id FROM developer WHERE name = ?', (developer,)
                ).fetchone()[0]

            crs.execute('''
                INSERT INTO developer_window(
                    developer_id,
                    window_id
                )VALUES(?,?)''', (
                    developer_id,
                    window_id
                )
            )

            for patch in patches:
                old_patch_len = len(seen_patches)
                seen_patches.add(patch)

                if len(seen_patches) != old_patch_len:
                    crs.execute('''
                        INSERT INTO patch(
                            url
                        )VALUES(?)''', (
                            patch,
                        )
                    )
                    patch_id = crs.lastrowid
                else:
                    patch_id = crs.execute(
                        'SELECT id FROM patch WHERE url = ?', (patch,)
                    ).fetchone()[0]

                try:
                    crs.execute('''
                        INSERT INTO developer_patch(
                            developer_id,
                            patch_id
                        )VALUES(?,?)''', (
                            developer_id,
                            patch_id
                        )
                    )
                except sqlite3.IntegrityError:
                    pass # meh

                try:
                    crs.execute('''
                        INSERT INTO patch_window(
                            window_id,
                            patch_id
                        )VALUES(?,?)''', (
                            window_id,
                            patch_id
                        )
                    )
                except sqlite3.IntegrityError:
                    pass # CF: '2020-01-23T19:00+00:00'

        # Commit after each window
        conn.commit()
